//
//  MCFirstViewController.m
//  MagicCubeApp
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import "MCFirstViewController.h"

@interface MCFirstViewController ()

@end

@implementation MCFirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
