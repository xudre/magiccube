//
//  MCAppDelegate.h
//  MagicCubeApp
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MagicCube/MagicCube.h>

@interface MCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
