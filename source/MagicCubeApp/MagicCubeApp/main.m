//
//  main.m
//  MagicCubeApp
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MCAppDelegate class]));
    }
}
