//
//  MCFirstViewController.h
//  MagicCubeApp
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import <MagicCube/MagicCube.h>

@interface MCFirstViewController : MagicCubeViewController

@end
