//
//  MagicCubeViewController.h
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MagicCubeViewController : UIViewController
{
    
}

@property int gestureRecognizers;
@property BOOL gestureEnabled;
@property float transitionTime;

- (void)willStartCubeRotation:(NSString *)direction destination:(UIViewController *)destination;
- (void)didStartCubeRotation:(NSString *)direction destination:(UIViewController *)destination;
- (void)didEndCubeRotation:(NSString *)direction destination:(UIViewController *)destination;

@end
