//
//  MagicCubeViewController.m
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import "MagicCubeViewController.h"

@interface MagicCubeViewController()
{
    
}

@end

@implementation MagicCubeViewController

#pragma mark - Initialization

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *sourceViewControlle = (UIViewController *) [segue sourceViewController];
    UIViewController *destinViewControlle = (UIViewController *) [segue destinationViewController];
    
    destinViewControlle.view.bounds = sourceViewControlle.view.bounds;
    [destinViewControlle.view setNeedsLayout];
}

- (void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
}

#pragma mark - Animation notifications

- (void)willStartCubeRotation:(NSString *)direction destination:(UIViewController *)destination
{
    
}

- (void)didStartCubeRotation:(NSString *)direction destination:(UIViewController *)destination
{
    
}

- (void)didEndCubeRotation:(NSString *)direction destination:(UIViewController *)destination
{
    
}

@end
