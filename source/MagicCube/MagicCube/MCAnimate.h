//
//  MCAnimate.h
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/CALayer.h>
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransform3D.h>

@interface MCAnimate : NSObject

+ (CALayer *)layerFromView:(UIView *)someView;
+ (CALayer *)layerFromView:(UIView *)someView withAnchor:(CGPoint)point;
+ (CATransform3D)transformWithTranslation:(Vector3D)translation andRotation:(Vector3D)rotation;

@end
