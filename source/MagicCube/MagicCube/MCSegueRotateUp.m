//
//  MCSegueRotateUp.m
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import "MCSegueRotateUp.h"

@interface MCSegueRotateUp()
{
    CALayer *animLayer;
    UIView *_animView;
    UIView *_origView;
}

@property (strong) UIView *animView;
@property (strong) UIView *origView;

@end

@implementation MCSegueRotateUp
{
    
}

- (void)perform
{
    UIViewController *sourceViewController = (UIViewController *) [self sourceViewController];
    UIViewController *destinViewController = (UIViewController *) [self destinationViewController];
    UIView *sourceView = sourceViewController.view;
    UIView *destinView = destinViewController.view;
    CGPoint fromPoint  = CGPointMake(.0f, .0f);
    CGPoint destPoint  = CGPointMake(.0f, 1.f);
    animLayer = [CALayer layer];
    CALayer *fromLayer = [MCAnimate layerFromView:sourceView withAnchor:fromPoint];
    CALayer *destLayer = [MCAnimate layerFromView:destinView withAnchor:destPoint];
    CAGradientLayer *fromFX = [CAGradientLayer layer];
    CAGradientLayer *destFX = [CAGradientLayer layer];
    
    float orientation = sourceView.bounds.size.width / sourceView.bounds.size.height;
    
    self.origView = sourceView;
    self.animView = [[UIView alloc] initWithFrame:sourceView.frame];
    self.animView.backgroundColor = [UIColor blackColor];
    
    [sourceViewController setView:self.animView];
    
    float duration = kMCTransitionTime;
    
    if ([sourceViewController isKindOfClass:[MagicCubeViewController class]])
    {
        if (((MagicCubeViewController *) sourceViewController).transitionTime > 0.f)
        {
            duration = ((MagicCubeViewController *) sourceViewController).transitionTime;
        }
    }
    
    // Animation
    CATransform3D perspective = CATransform3DIdentity;
    
    perspective.m34 = 1.f / kMCFocalLength;
    
    animLayer.name  = @"transitionAnimation";
    animLayer.frame = sourceView.bounds;
    
    if (orientation > 1.f)
    {
        animLayer.affineTransform = CGAffineTransformMakeRotation(-M_PI_2);
        animLayer.position = CGPointMake(animLayer.position.y, animLayer.position.x);
    }
    
    animLayer.sublayerTransform = perspective;
    
    fromFX.startPoint   = CGPointMake(.5f, .0f);
    fromFX.endPoint     = CGPointMake(.5f, 1.f);
    fromFX.anchorPoint  = fromPoint;
    fromFX.frame        = sourceView.bounds;
    fromFX.colors = [NSArray arrayWithObjects:
                     (id) [[UIColor colorWithRed:.0f green:.0f blue:.0f alpha:.0f] CGColor],
                     (id) [[UIColor colorWithRed:.0f green:.0f blue:.0f alpha:1.f] CGColor],
                     nil];
    
    destFX.startPoint   = CGPointMake(.5f, 1.f);
    destFX.endPoint     = CGPointMake(.5f, .0f);
    destFX.anchorPoint  = destPoint;
    destFX.frame        = sourceView.bounds;
    destFX.colors = [NSArray arrayWithObjects:
                     (id) [[UIColor colorWithRed:.0f green:.0f blue:.0f alpha:.0f] CGColor],
                     (id) [[UIColor colorWithRed:.0f green:.0f blue:.0f alpha:1.f] CGColor],
                     nil];
    
    CAKeyframeAnimation *fromAnim     = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CAKeyframeAnimation *destAnim     = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CAKeyframeAnimation *fromFXAnim   = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CAKeyframeAnimation *destFXAnim   = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CABasicAnimation *fromFXFade      = [CABasicAnimation animationWithKeyPath:@"opacity"];
    CABasicAnimation *destFXFade      = [CABasicAnimation animationWithKeyPath:@"opacity"];
    CAAnimationGroup *fromFXAnimGroup = [CAAnimationGroup animation];
    CAAnimationGroup *destFXAnimGroup = [CAAnimationGroup animation];
    
    destAnim.delegate = self;
    
    fromAnim.duration   = duration;
    destAnim.duration   = duration;
    fromFXAnim.duration = duration;
    destFXAnim.duration = duration;
    fromFXFade.duration = duration;
    destFXFade.duration = duration;
    fromFXAnimGroup.duration = duration;
    destFXAnimGroup.duration = duration;
    
    fromAnim.fillMode   = kCAFillModeForwards;
    destAnim.fillMode   = kCAFillModeForwards;
    fromFXAnim.fillMode = kCAFillModeForwards;
    destFXAnim.fillMode = kCAFillModeForwards;
    fromFXFade.fillMode = kCAFillModeForwards;
    destFXFade.fillMode = kCAFillModeForwards;
    fromFXAnimGroup.fillMode = kCAFillModeForwards;
    destFXAnimGroup.fillMode = kCAFillModeForwards;
    
    fromAnim.removedOnCompletion   = NO;
    destAnim.removedOnCompletion   = NO;
    fromFXAnim.removedOnCompletion = NO;
    destFXAnim.removedOnCompletion = NO;
    fromFXFade.removedOnCompletion = NO;
    destFXFade.removedOnCompletion = NO;
    fromFXAnimGroup.removedOnCompletion = NO;
    destFXAnimGroup.removedOnCompletion = NO;
    
    float middleH = animLayer.bounds.size.height / 2;
    
    // -- From keyframing
    CATransform3D fromFirstFrame  = CATransform3DIdentity;
    CATransform3D fromSecondFrame = CATransform3DMakeTranslation(.0f, middleH, -middleH);
    CATransform3D fromLastFrame   = CATransform3DMakeTranslation(.0f, middleH * 2, .0f);
    
    fromSecondFrame = CATransform3DRotate(fromSecondFrame, -M_PI_4, 1.f, .0f, .0f);
    fromLastFrame   = CATransform3DRotate(fromLastFrame, -M_PI_2, 1.f, .0f, .0f);
    
    // -- Destination keyframing
    CATransform3D toFirstFrame  = CATransform3DMakeTranslation(.0f, -middleH * 2, .0f);
    CATransform3D toSecondFrame = CATransform3DMakeTranslation(.0f, -middleH, -middleH);
    CATransform3D toLastFrame   = CATransform3DIdentity;
    
    toFirstFrame  = CATransform3DRotate(toFirstFrame, M_PI_2, 1.f, .0f, .0f);;
    toSecondFrame = CATransform3DRotate(toSecondFrame, M_PI_4, 1.f, .0f, .0f);
    
    // -- Animation keyframes order
    fromAnim.values = [NSArray arrayWithObjects:
                       [NSValue valueWithCATransform3D:fromFirstFrame],
                       [NSValue valueWithCATransform3D:fromSecondFrame],
                       [NSValue valueWithCATransform3D:fromLastFrame],
                       nil];
    
    destAnim.values = [NSArray arrayWithObjects:
                       [NSValue valueWithCATransform3D:toFirstFrame],
                       [NSValue valueWithCATransform3D:toSecondFrame],
                       [NSValue valueWithCATransform3D:toLastFrame],
                       nil];
    
    fromFXAnim.values = [NSArray arrayWithArray:fromAnim.values];
    destFXAnim.values = [NSArray arrayWithArray:destAnim.values];
    
    fromFXFade.fromValue = [NSNumber numberWithFloat:.0f];
    fromFXFade.toValue   = [NSNumber numberWithFloat:1.f];
    
    destFXFade.fromValue = [NSNumber numberWithFloat:1.f];
    destFXFade.toValue   = [NSNumber numberWithFloat:.0f];
    
    fromFXAnimGroup.animations = [NSArray arrayWithObjects:fromFXAnim, fromFXFade, nil];
    destFXAnimGroup.animations = [NSArray arrayWithObjects:destFXAnim, destFXFade, nil];
    
    [animLayer addSublayer:fromLayer];
    [animLayer addSublayer:destLayer];
    [animLayer addSublayer:fromFX];
    [animLayer addSublayer:destFX];
    
    [self.animView.layer addSublayer:animLayer];
    
    [CATransaction begin];
    
    [fromLayer addAnimation:fromAnim forKey:@"kFromTransform"];
    [destLayer addAnimation:destAnim forKey:@"kDestTransform"];
    [fromFX addAnimation:fromFXAnimGroup forKey:@"kFromFXAnim"];
    [destFX addAnimation:destFXAnimGroup forKey:@"kDestFXAnim"];
    
    [CATransaction commit];
}

#pragma mark - Animation delegate

- (void)animationDidStart:(CAAnimation *)theAnimation
{
    
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)ended
{
    [self.sourceViewController setView:self.origView];
    [self.sourceViewController presentModalViewController:self.destinationViewController animated:NO];
    
    if (ended)
    {
        
    }
    else
    {
        
    }
    
    [animLayer removeFromSuperlayer];
    self.animView = nil;
    self.origView = nil;
}

@end
