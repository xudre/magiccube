//
//  MagicCube.h
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#ifndef MagicCube_MagicCube_h
#define MagicCube_MagicCube_h

const float kMCTransitionTime = .5f;
const float kMCFocalLength = -850.f;

typedef struct { float x; float y; float z; float w; } Vector3D;

#import "MagicCubeViewController.h"
#import "MCSegueRotateUp.h"
#import "MCSegueRotateRight.h"
#import "MCSegueRotateDown.h"
#import "MCSegueRotateLeft.h"

#endif
