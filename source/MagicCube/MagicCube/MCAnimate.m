//
//  MCAnimate.m
//  MagicCube
//
//  Created by Pedro Xudre on 17/5/13.
//  Copyright (c) 2013 The Pipe Cat. All rights reserved.
//

#import "MCAnimate.h"

@interface MCAnimate()
{
    
}

@end

@implementation MCAnimate

+ (CALayer *)layerFromView:(UIView *)someView
{
    return [self layerFromView:someView withAnchor:CGPointMake(.5f, .5f)];
}

+ (CALayer *)layerFromView:(UIView *)someView withAnchor:(CGPoint)point
{
    CALayer *layer = [CALayer layer];
    layer.anchorPoint = point;
    layer.frame = someView.bounds;
    
    UIGraphicsBeginImageContext(someView.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    [someView.layer renderInContext:ctx];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    layer.contents = (id) viewImage.CGImage;
    
    return layer;
}

+ (CATransform3D)transformWithTranslation:(Vector3D)translation andRotation:(Vector3D)rotation
{
    CATransform3D transform = CATransform3DMakeTranslation(translation.x * translation.w,
                                                           translation.y * translation.w,
                                                           translation.z * translation.w);
    
    transform = CATransform3DRotate(transform, DegreesToRadians(rotation.x), rotation.w, .0f, .0f);
    transform = CATransform3DRotate(transform, DegreesToRadians(rotation.y), .0f, rotation.w, .0f);
    transform = CATransform3DRotate(transform, DegreesToRadians(rotation.z), .0f, .0f, rotation.w);
    
    return transform;
}

@end
